# Swaps - Mobile App

This repo contains both mobile and server codes. On start, don't forget to:
 
### swap-api
- Download dependencies.
	> compser install
### swap-app
- Download dependencies.
	> npm install
- Run live server.
	> ionic serve
