import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './splashscreen/splashscreen.module#SplashscreenPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'profile', loadChildren: './profile/profile-routes.module#ProfileRoutesPageModule' },
  { path: 'home-title', loadChildren: './home-title/home-title-routes.module#HomeTitleRoutesPageModule' },
  { path: 'message', loadChildren: './messages/messages.module#MessagesPageModule' },
  { path: 'matching', loadChildren: './view/matching/matching.module#MatchingPageModule' },
  { path: 'matched', loadChildren: './view/matched/matched.module#MatchedPageModule' },
  { path: 'nomatch', loadChildren: './view/nomatch/nomatch.module#NomatchPageModule' },
  { path: 'swaps', loadChildren: './swaps/swaps.module#SwapsPageModule' },
  { path: 'near', loadChildren: './near/near.module#NearPageModule' },
  { path: 'more', loadChildren: './more/more.module#MorePageModule' },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}