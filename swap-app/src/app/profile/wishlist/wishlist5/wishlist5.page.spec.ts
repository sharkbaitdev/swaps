import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Wishlist5Page } from './wishlist5.page';

describe('Wishlist5Page', () => {
  let component: Wishlist5Page;
  let fixture: ComponentFixture<Wishlist5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Wishlist5Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Wishlist5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
