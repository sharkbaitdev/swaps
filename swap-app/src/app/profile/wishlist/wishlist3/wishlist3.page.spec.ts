import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Wishlist3Page } from './wishlist3.page';

describe('Tab1Page', () => {
  let component: Wishlist3Page;
  let fixture: ComponentFixture<Wishlist3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Wishlist3Page],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Wishlist3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
