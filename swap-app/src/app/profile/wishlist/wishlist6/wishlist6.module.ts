import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Wishlist6Page } from './wishlist6.page';

const routes: Routes = [
  {
    path: '',
    component: Wishlist6Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Wishlist6Page]
})
export class Wishlist6PageModule {}
