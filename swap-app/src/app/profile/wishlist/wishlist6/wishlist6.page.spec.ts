import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Wishlist6Page } from './wishlist6.page';

describe('Wishlist6Page', () => {
  let component: Wishlist6Page;
  let fixture: ComponentFixture<Wishlist6Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Wishlist6Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Wishlist6Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
