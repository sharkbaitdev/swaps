import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Wishlist2Page } from './wishlist2.page';
import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Wishlist2Page }])
  ],
  declarations: [Wishlist2Page]
})
export class Wishlist2PageModule {}
