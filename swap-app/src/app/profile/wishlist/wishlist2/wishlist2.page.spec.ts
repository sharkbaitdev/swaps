import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Wishlist2Page } from './wishlist2.page';

describe('Tab1Page', () => {
  let component: Wishlist2Page;
  let fixture: ComponentFixture<Wishlist2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Wishlist2Page],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Wishlist2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
