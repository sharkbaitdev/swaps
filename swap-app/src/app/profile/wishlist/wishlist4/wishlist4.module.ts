import { IonicModule } from '@ionic/angular';
import { RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Wishlist4Page } from './wishlist4.page';
import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: Wishlist4Page
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Wishlist4Page]
})
export class Wishlist4PageModule {}
