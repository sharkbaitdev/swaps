import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Wishlist4Page } from './wishlist4.page';

describe('Tab1Page', () => {
  let component: Wishlist4Page;
  let fixture: ComponentFixture<Wishlist4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Wishlist4Page],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Wishlist4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
