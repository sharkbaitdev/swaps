import { Component } from '@angular/core';

@Component({
  selector: 'app-wishlist4',
  templateUrl: 'wishlist4.page.html',
  styleUrls: ['wishlist4.page.scss']
})
export class Wishlist4Page {

  public isUploaded = false;
  public selectOptionColor = {
    title: 'Color'
  };

  onFileInput($event) {

    let myFile = $event.target.files;
    /*let reader = new FileReader();
    reader.readAsText(myFile);
    reader.onload = (ev) => {
      this.files = reader.result;
    } */

    if (myFile.length > 0){
      this.isUploaded = true;
      console.log(myFile)
    }
  }

}
