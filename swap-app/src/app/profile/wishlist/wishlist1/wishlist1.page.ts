import { Component } from '@angular/core';

@Component({
  selector: 'app-wishlist1',
  templateUrl: 'wishlist1.page.html',
  styleUrls: ['wishlist1.page.scss']
})
export class Wishlist1Page {

  public isUploaded = false;

  onFileInput($event) {

    let myFile = $event.target.files;
    /*let reader = new FileReader();
    reader.readAsText(myFile);
    reader.onload = (ev) => {
      this.files = reader.result;
    } */

    if (myFile.length > 0){
      this.isUploaded = true;
      console.log(myFile)
    }
  }

}
