import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Wishlist1Page } from './wishlist1.page';

describe('Tab1Page', () => {
  let component: Wishlist1Page;
  let fixture: ComponentFixture<Wishlist1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Wishlist1Page],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Wishlist1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
