import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Wishlist3ChoosePage } from './wishlist3-choose.page';

const routes: Routes = [
  {
    path: '',
    component: Wishlist3ChoosePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Wishlist3ChoosePage]
})
export class Wishlist3ChoosePageModule {}
