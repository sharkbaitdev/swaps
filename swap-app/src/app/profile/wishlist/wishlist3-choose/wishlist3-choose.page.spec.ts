import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Wishlist3ChoosePage } from './wishlist3-choose.page';

describe('Wishlist3ChoosePage', () => {
  let component: Wishlist3ChoosePage;
  let fixture: ComponentFixture<Wishlist3ChoosePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Wishlist3ChoosePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Wishlist3ChoosePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
