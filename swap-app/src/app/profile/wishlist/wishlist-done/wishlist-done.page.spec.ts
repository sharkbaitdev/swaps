import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WishlistDonePage } from './wishlist-done.page';

describe('WishlistDonePage', () => {
  let component: WishlistDonePage;
  let fixture: ComponentFixture<WishlistDonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WishlistDonePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WishlistDonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
