import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: './profile.module#ProfilePageModule'
  },
  {
    path: 'wishlist1',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist1/wishlist1.module#Wishlist1PageModule'
      }
    ]
  },
  {
    path: 'wishlist2',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist2/wishlist2.module#Wishlist2PageModule'
      }
    ]
  },
  {
    path: 'wishlist3',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist3/wishlist3.module#Wishlist3PageModule'
      }
    ]
  },
  {
    path: 'wishlist3-choose/:tag',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist3-choose/wishlist3-choose.module#Wishlist3ChoosePageModule'
      }
    ]
  },
  {
    path: 'wishlist4',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist4/wishlist4.module#Wishlist4PageModule'
      }
    ]
  },{
    path: 'wishlist5',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist5/wishlist5.module#Wishlist5PageModule'
      }
    ]
  },{
    path: 'wishlist6',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist6/wishlist6.module#Wishlist6PageModule'
      }
    ]
  },{
    path: 'wishlist-done',
    children: [
      {
        path: '',
        loadChildren: './wishlist/wishlist-done/wishlist-done.module#WishlistDonePageModule'
      }
    ]
  },
  {
    path: 'bundles',
    children: [
      { 
        path: '', 
        loadChildren: './bundles/bundles.module#BundlesPageModule' 
      },
    ]
  },
  {
    path: 'items',
    children: [
      { 
        path: '', 
        loadChildren: './items/items.module#ItemsPageModule' 
      },
    ]
  },
  {
    path: 'views',
    children: [
      { 
        path: 'item', 
        loadChildren: './views/item/item.module#ItemPageModule' 
      },
      { 
        path: 'bundle', 
        loadChildren: './views/bundle/bundle.module#BundlePageModule' 
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ProfilePageRoutingModule {}
