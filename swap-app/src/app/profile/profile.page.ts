import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PopupPage } from './popup/popup.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public strProfilePhoto = "assets/icon/icon_main.png";
  public strProfileName = "Christine Joyce";
  public strProfileCity = "Taguig City";
  public intSwapHost = 4;
  public intPeopleReached = 20;
  public intClothesSold = 6;

  constructor(
    public modalController: ModalController,
    private router: Router
  ) { }

  async presentModal() {
    let modal = await this.modalController.create({
      component: PopupPage,
      componentProps: { value: 123 },
      cssClass: 'select-modal'
    });

    modal.onDidDismiss().then((r) => {
      if (r.data != '') {
        this.router.navigateByUrl('/profile/' + r.data);
      }
    });

    return await modal.present();
  }

  ngOnInit() {
  }

}
