import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/profile/items/item1',
  },
  {
    path: 'item1',
    children: [
      {
        path: '',
        loadChildren: './items1/step1i.module#Step1iPageModule'
      }
    ]
  },
  {
    path: 'item2',
    children: [
      {
        path: '',
        loadChildren: './items2/step2i.module#Step2iPageModule'
      }
    ]
  },
  {
    path: 'item3',
    children: [
      {
        path: '',
        loadChildren: './items3/step3i.module#Step3iPageModule'
      },
      { 
        path: 'choose/:tag', 
        loadChildren: './items3/choose/choose.module#ChoosePageModule'
      }
    ]
  },
  {
    path: 'item4',
    children: [
      {
        path: '',
        loadChildren: './items4/step4i.module#Step4iPageModule'
      },
    ]
  },
  {
    path: 'item5',
    children: [
      {
        path: '',
        loadChildren: './items5/step5i.module#Step5iPageModule'
      }
    ]
  },
  {
    path: 'item6',
    children: [
      {
        path: '',
        loadChildren: './items6/step6i.module#Step6iPageModule'
      },
      { 
        path: 'choose/:tag', 
        loadChildren: './items6/choose/choose.module#ChoosePageModule'
      }
    ]
  },
  { 
    path: 'itemsdone', 
    loadChildren: './done/donei.module#DoneiPageModule' 
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ItemsPageRoutingModule {}
