import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/profile/bundles/step1',
  },
  {
    path: 'step1',
    children: [
      {
        path: '',
        loadChildren: './step1/step1.module#Step1PageModule'
      }
    ]
  },
  {
    path: 'step2',
    children: [
      {
        path: '',
        loadChildren: './step2/step2.module#Step2PageModule'
      }
    ]
  },
  {
    path: 'step3',
    children: [
      {
        path: '',
        loadChildren: './step3/step3.module#Step3PageModule'
      }
    ]
  },
  {
    path: 'step4',
    children: [
      { 
        path: 'choose/:tag', 
        loadChildren: './step4/choose/choose.module#ChoosePageModule'
      },
      {
        path: ':tag',
        loadChildren: './step4/step4.module#Step4PageModule'
      },
    ]
  },
  {
    path: 'step5',
    children: [
      {
        path: ':tag',
        loadChildren: './step5/step5.module#Step5PageModule'
      }
    ]
  },
  { 
    path: 'congrats', 
    loadChildren: './congrats/congrats.module#CongratsPageModule' 
  }
];
/**
 * 
 *   { path: 'step3', loadChildren: './profile/bundles/step3/step3.module#Step3PageModule' },
  { path: 'step4', loadChildren: './profile/bundles/step4/step4.module#Step4PageModule' },
  { path: 'choose', loadChildren: './profile/bundles/step3/choose/choose.module#ChoosePageModule' },
  { path: 'step5', loadChildren: './profile/bundles/step5/step5.module#Step5PageModule' },
 */

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BundlesPageRoutingModule {}
