import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Step2Page } from './step2.page';
import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';
import { PopupPage } from './popup/popup.page';

const routes: Routes = [
  {
    path: '',
    component: Step2Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Step2Page, PopupPage],
  entryComponents: [
    PopupPage
  ]
})
export class Step2PageModule {}
