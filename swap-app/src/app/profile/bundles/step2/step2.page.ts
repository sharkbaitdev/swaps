import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PopupPage } from './popup/popup.page';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.page.html',
  styleUrls: ['./step2.page.scss'],
})
export class Step2Page implements OnInit {

  public objData = {
    top_size: '',
    bottom_size: '',
  }

  constructor(
    public modalController: ModalController,
    private router: Router
  ) { }

  async presentModal() {
    let modal = await this.modalController.create({
      component: PopupPage,
      componentProps: { value: 123 },
      cssClass: 'select-modal'
    });

    return await modal.present();
  }

  ngOnInit() {
  }

}
