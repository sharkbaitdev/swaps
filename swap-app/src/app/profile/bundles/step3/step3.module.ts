import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Step3Page } from './step3.page';
import { MatButtonModule, MatIconModule, MatInputModule, MatRadioModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: Step3Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Step3Page]
})
export class Step3PageModule {}
