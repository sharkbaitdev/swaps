import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Step5Page } from './step5.page';
import { MatButtonModule, MatIconModule, MatInputModule, MatRadioModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: Step5Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Step5Page]
})
export class Step5PageModule {}
