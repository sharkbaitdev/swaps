import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-step5',
  templateUrl: './step5.page.html',
  styleUrls: ['./step5.page.scss'],
})
export class Step5Page implements OnInit {
  public strTag = '';
  public objLabels = {
    'bundle_a': {label: 'Bundle Type A'},
    'bundle_b': {label: 'Bundle Type B'}
  }
  constructor(
    private route: ActivatedRoute
  ) {
      this.route.params.subscribe(params => {
        this.strTag = params['tag'];
      });
    }

  ngOnInit() {
  }

}
