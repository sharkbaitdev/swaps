import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from '../../../../a-services/register.service';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.page.html',
  styleUrls: ['./choose.page.scss'],
})
export class ChoosePage implements OnInit {

  public strTag = '';
  public arNecklines = [
    {'id': 'boat', 'name': 'Boat', 'img': 'Boat.png', 'chosen': false},
    {'id': 'camisole', 'name': 'Camisole', 'img': 'Camisole.png', 'chosen': false},
    {'id': 'cardigan', 'name': 'Cardigan', 'img': 'Cardigan.png', 'chosen': false},
    {'id': 'cowl', 'name': 'Cowl', 'img': 'Cowl.png', 'chosen': false},
    {'id': 'decollete', 'name': 'Decollete', 'img': 'Decollete.png', 'chosen': false},
    {'id': 'Florentine', 'name': 'Florentine', 'img': 'Florentine.png', 'chosen': false},
    {'id': 'Funnel', 'name': 'Funnel', 'img': 'Funnel.png', 'chosen': false},
    {'id': 'Gathered', 'name': 'Gathered', 'img': 'Gathered.png', 'chosen': false},
    {'id': 'Halter', 'name': 'Halter', 'img': 'Halter.png', 'chosen': false},
    {'id': 'Horseshoe', 'name': 'Horseshoe', 'img': 'Horseshoe.png', 'chosen': false},
    {'id': 'Jewel', 'name': 'Jewel', 'img': 'Jewel.png', 'chosen': false},
    {'id': 'Keyhole', 'name': 'Keyhole', 'img': 'Keyhole.png', 'chosen': false},
    {'id': 'One-shoulder', 'name': 'One-shoulder', 'img': 'One shoulder.png', 'chosen': false},
    {'id': 'Plunging', 'name': 'Plunging', 'img': 'Plunging.png', 'chosen': false},
    {'id': 'Sabrina', 'name': 'Sabrina', 'img': 'Sabrina.png', 'chosen': false},
    {'id': 'Scoop', 'name': 'Scoop', 'img': 'Scoop.png', 'chosen': false},
    {'id': 'Slit', 'name': 'Slit', 'img': 'Slit.png', 'chosen': false},
    {'id': 'Square', 'name': 'Square', 'img': 'Square.png', 'chosen': false},
    {'id': 'Strapless', 'name': 'Strapless', 'img': 'Strapless.png', 'chosen': false},
    {'id': 'Surplice', 'name': 'Surplice', 'img': 'Surplice.png', 'chosen': false},
    {'id': 'U', 'name': 'U', 'img': 'U.png', 'chosen': false},
    {'id': 'V', 'name': 'V', 'img': 'V.png', 'chosen': false},
    {'id': 'Yoke', 'name': 'Yoke', 'img': 'Yoke.png', 'chosen': false},

  ];
  public arNeckline = [];

  public objLabels = {
    'shoulder_cut': { label: 'Choose shoulder cut'},
    'necklines': { label: 'Choose neckline', list: this.arNecklines},
    'collar': { label: 'Choose collar'},
    'sleeve_length': { label: 'Choose sleeve length'},
    'skirt_length': { label: 'Choose skirit length'},
    'rise': { label: 'Choose rise'},
  }

  constructor(
    public registerService: RegisterService,
    private route: ActivatedRoute
  ) {
      this.route.params.subscribe(params => {
        this.strTag = params.tag; 
      });
    }

  ngOnInit() {}

  chooseThis(chosenObj) {
    let objId = this.objLabels[this.strTag]['list'].findIndex(item => (item.id == chosenObj.id));

    if (objId != -1) {
      if (this.objLabels[this.strTag]['list'][objId]['chosen']) {
        this.objLabels[this.strTag]['list'][objId]['chosen'] = false;
      } else {
        this.objLabels[this.strTag]['list'][objId]['chosen'] = true;
      }
    }
  }

}
