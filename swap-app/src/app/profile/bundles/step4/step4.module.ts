import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Step4Page } from './step4.page';
import { MatButtonModule, MatIconModule, MatInputModule, MatRadioModule, MatCheckboxModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: Step4Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatCheckboxModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Step4Page]
})
export class Step4PageModule {}
