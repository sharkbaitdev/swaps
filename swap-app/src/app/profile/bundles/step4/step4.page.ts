import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-step4',
  templateUrl: './step4.page.html',
  styleUrls: ['./step4.page.scss'],
})
export class Step4Page implements OnInit {

  public strType = '';
  public bOpenOcassion = false;
  public bOpenColor = false;
  public bOpenStyle = false;
  public bOpenDetail = false;

  public arDetail = [
    {id: 'shoulder_cut', name: 'Shoulder cut'},
    {id: 'necklines', name: 'Neckline'},
    {id: 'sleeve_length', name: 'Sleeve length'},
    {id: 'skirt_length', name: 'Skirt length'},
    {id: 'rise', name: 'Rise'},
    
  ];

  public arStyle = [
    {included: false, id: 'minimalistic', name: 'Minimalistic'},
    {included: false, id: 'experimental', name: 'Experimental'},
    {included: false, id: 'colorful', name: 'Colorful'},
    {included: false, id: 'satement_making', name: 'Statement-making'},
    {included: false, id: 'feminine', name: 'Feminine'},
    {included: false, id: 'straightforward', name: 'Straightforward'},
    {included: false, id: 'understated', name: 'Understated'},
    {included: false, id: 'sophisticated', name: 'Sophisticated'},
    {included: false, id: 'modern', name: 'Modern'},
    {included: false, id: 'sultry', name: 'Sultry'},
    {included: false, id: 'effortless', name: 'Effortless'},
    {included: false, id: 'classic', name: 'Classic'}
  ];

  public arColor = [
    {included: false, id: 'black', name: 'Black'},
    {included: false, id: 'grey', name: 'Grey'},
    {included: false, id: 'white', name: 'White'},
    {included: false, id: 'ivory', name: 'Ivory'},
    {included: false, id: 'tan', name: 'Tan'},
    {included: false, id: 'brown', name: 'Brown'},
    {included: false, id: 'purple', name: 'Purple'},
    {included: false, id: 'blue', name: 'Blue'},
    {included: false, id: 'teal', name: 'Teal'},
    {included: false, id: 'green', name: 'Green'},
    {included: false, id: 'red', name: 'Red'},
    {included: false, id: 'pink', name: 'Pink'},
    {included: false, id: 'orange', name: 'Orange'},
    {included: false, id: 'yellow', name: 'Yellow'},
  ];
  public arOccasion = [
    {included: false, id: 'cocktail', name: 'Cocktail Parties'},
    {included: false, id: 'fancy', name: 'Fancy Parties'},
    {included: false, id: 'formal', name: 'Formal Occasions and Night Outs'},
    {included: false, id: 'lounging', name: 'Lounging'},
    {included: false, id: 'meetings', name: 'Office/ Business Meetings'},
    {included: false, id: 'religious', name: 'Religious Ceremonies'},
    {included: false, id: 'school', name: 'School'},
    {included: false, id: 'travel_cold', name: 'Travel (Cold Climate)'},
    {included: false, id: 'travel_hot', name: 'Travel (Hot Climate)'},
    {included: false, id: 'workout', name: 'Working Out'},
  ];

  public objLabels = {
    'bundle_a': {label: 'Bundle Type A', title: "Select Category"},
    'bundle_b': {label: 'Bundle Type B', title: "Choose Theme"}
  }

  constructor(
    private route: ActivatedRoute
  ) {
      this.route.params.subscribe(params => {
        this.strType = params['tag'];
      });
    }

  ngOnInit() {
  }

  toggleOccasion() {
    this.bOpenOcassion = (this.bOpenOcassion) ? false : true;
  }
  toggleColor() {
    this.bOpenColor = (this.bOpenColor) ? false : true;
  }
  toggleStyle() {
    this.bOpenStyle = (this.bOpenStyle) ? false : true;
  }
  toggleDetail() {
    this.bOpenDetail = (this.bOpenDetail) ? false : true;
  }

}
