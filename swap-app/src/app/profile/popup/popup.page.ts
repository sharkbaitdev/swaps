import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.page.html',
  styleUrls: ['./popup.page.scss'],
})
export class PopupPage implements OnInit {

  // "value" passed in componentProps
  @Input() value: number;

  constructor(navParams: NavParams, private modalCtrl:ModalController) {
    // componentProps can also be accessed at construction time using NavParams

    console.log(navParams.data);
  }

  ngOnInit() {
  }

  closeModal(strWhere)
  {
    this.modalCtrl.dismiss(strWhere);
  }

}
