import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-title',
  templateUrl: './home-title.page.html',
  styleUrls: ['./home-title.page.scss'],
})
export class HomeTitlePage implements OnInit {

  constructor(private router: Router) {
    setTimeout(() => {
      this.router.navigateByUrl('/home-title/page1');
    }, 2000);
  }

  ngOnInit() {
  }

}
