import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: './home-title.module#HomeTitlePageModule'
      }
    ]
  },
  {
    path: 'page1',
    children: [
      {
        path: '',
        loadChildren: './page1/page1.module#Page1PageModule'
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeTitlePageRoutingModule {}
