import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTitlePage } from './home-title.page';

describe('HomeTitlePage', () => {
  let component: HomeTitlePage;
  let fixture: ComponentFixture<HomeTitlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeTitlePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTitlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
