import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeTitlePageRoutingModule } from './home-title.router.module';


@NgModule({
  imports: [
    CommonModule,
    HomeTitlePageRoutingModule,
  ],
})
export class HomeTitleRoutesPageModule {}
