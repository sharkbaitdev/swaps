import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwapsPage } from './swaps.page';

describe('SwapsPage', () => {
  let component: SwapsPage;
  let fixture: ComponentFixture<SwapsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwapsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwapsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
