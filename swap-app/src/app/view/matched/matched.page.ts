import { Component, OnInit, ViewChild } from '@angular/core';
import { Content } from '@ionic/angular';

@Component({
  selector: 'app-matched',
  templateUrl: './matched.page.html',
  styleUrls: ['./matched.page.scss'],
})
export class MatchedPage implements OnInit {

  @ViewChild(Content) content: Content;

  scrollToTop() {
    this.content.scrollToTop();
  }

  constructor() { }

  ngOnInit() {
  }

}
