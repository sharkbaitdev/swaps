import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchedPage } from './matched.page';

describe('MatchedPage', () => {
  let component: MatchedPage;
  let fixture: ComponentFixture<MatchedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
