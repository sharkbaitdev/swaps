import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  public objWishlistData = {}

  public objItemType = {}

  constructor(private http: HttpClient) { }

  mergeData(objToBeMerged) {
    Object.assign(this.objWishlistData, objToBeMerged);
    console.log(this.objWishlistData);
  }

  getCurrentWishlistObject () {
    return this.objWishlistData;
  }

  getItemType() {
    return this.objItemType;
  }

  setItemType (objToBeMerged) {
    Object.assign(this.objItemType, objToBeMerged);
    console.log(this.objItemType);
  }

  wishlist() {

    let formData = new FormData();
/*
    let finalize = {
      name: this.objUserRegister['name'],
      email: this.objUserRegister['email'],
      password: this.objUserRegister['password'],
      location: this.objUserRegister['location']
    };

    delete this.objUserRegister['name'];
    delete this.objUserRegister['email'];
    delete this.objUserRegister['password'];
    delete this.objUserRegister['location'];

    */

    //finalize['other_info'] = this.objUserRegister;
    formData.append('body', JSON.stringify(this.objWishlistData));

    console.log(formData)
    //return this.http.post(environment.api_url + '/register', formData);
  }
}
