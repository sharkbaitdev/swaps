import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

    public objUserRegister = {};
    public objUserData = {};

    constructor(private http: HttpClient) { }

    mergeData(objToBeMerged) {
        Object.assign(this.objUserRegister, objToBeMerged);
    }

    getCurrentRegisterObject () {
        return this.objUserRegister;
    }


    // GETTERS AND SETTERS FOR LOGGED IN USER
    getUserData () {
      return this.objUserData;
    }

    setUserData (objData) {
      this.objUserData = objData;
    }

    login(objUserLogin) {
      
      let formData = new FormData();

      formData.append('body', JSON.stringify(objUserLogin));
      return this.http.post(environment.api_url + '/login', formData);
    }

    register() {

        let formData = new FormData();

        this.objUserRegister['common_clothes'] = [];

        if (this.objUserRegister['common_jackets'] != undefined) {
            this.objUserRegister['common_clothes'].push({"common_jackets" : this.objUserRegister['common_jackets']});
            delete this.objUserRegister['common_jackets'];
        }

        if (this.objUserRegister['common_dresses'] != undefined) {
            this.objUserRegister['common_clothes'].push({"common_dresses" : this.objUserRegister['common_dresses']});
            delete this.objUserRegister['common_dresses'];
        }

        if (this.objUserRegister['common_pants'] != undefined) {
            this.objUserRegister['common_clothes'].push({"common_pants" : this.objUserRegister['common_pants']});
            delete this.objUserRegister['common_pants'];
        }

        if (this.objUserRegister['common_shorts'] != undefined) {
            this.objUserRegister['common_clothes'].push({"common_shorts" : this.objUserRegister['common_shorts']});
            delete this.objUserRegister['common_shorts'];
        }

        if (this.objUserRegister['common_skirts'] != undefined) {
            this.objUserRegister['common_clothes'].push({"common_skirts" : this.objUserRegister['common_skirts']});
            delete this.objUserRegister['common_skirts'];
        }

        if (this.objUserRegister['common_tops'] != undefined) {
              this.objUserRegister['common_clothes'].push({"common_tops" : this.objUserRegister['common_tops']});
              delete this.objUserRegister['common_tops'];
        }

        delete this.objUserRegister['confirm_password'];

        formData.append('body', JSON.stringify(this.objUserRegister));

        return this.http.post(environment.api_url + '/register', formData);
    }  
}
