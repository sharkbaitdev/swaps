import { Component, OnInit, ViewChild } from '@angular/core';
import { Slides } from '@ionic/angular';
import { RegisterService } from '../../a-services/register.service';

@Component({
  selector: 'app-signup1',
  templateUrl: './signup1.page.html',
  styleUrls: ['./signup1.page.scss'],
})
export class Signup1Page implements OnInit {

  public objData = {
    name: '',
    email: '',
    password: '',
    confirm_password: '',
    location: '',
  }

  public hide = true;
  public hide1 = true;

  constructor(public registerService: RegisterService) { }

  ngOnInit() {}

  saveData() {
    this.registerService.mergeData(this.objData);
  }

}
