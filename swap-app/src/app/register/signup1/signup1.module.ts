import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { Signup1Page } from './signup1.page';
import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: Signup1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Signup1Page]
})
export class Signup1PageModule {}
