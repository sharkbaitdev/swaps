import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../../a-services/register.service';

@Component({
  selector: 'app-signup5',
  templateUrl: './signup5.page.html',
  styleUrls: ['./signup5.page.scss'],
})
export class Signup5Page implements OnInit {

  constructor(public registerService: RegisterService) { }

  ngOnInit() {
  }
  
  register() {
    this.registerService.register().subscribe(data => {
      console.log(data);
    })
  }

}
