import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from '../../../a-services/register.service';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.page.html',
  styleUrls: ['./choose.page.scss'],
})
export class ChoosePage implements OnInit {

  public strTag = '';

  public bShow = false;

  public arTops = [
    {'id': 'blouson', 'name': 'Blouson', 'img': 'top 7.jpg', 'chosen': false},
    {'id': 'bustier', 'name': 'Bustier', 'img': 'top 1.jpg', 'chosen': false},
    {'id': 'button_down', 'name': 'Button-down', 'img': 'top 4.jpg', 'chosen': false},
    {'id': 'camisole', 'name': 'Camisole', 'img': 'top 8.jpg', 'chosen': false},
    {'id': 'cold_shoulder', 'name': 'Cold Shoulder', 'img': 'top 2.jpg', 'chosen': false},
    {'id': 'halter', 'name': 'Halter', 'img': 'top 3.jpg', 'chosen': false},
    {'id': 'overblouse', 'name': 'Overblouse', 'img': 'top 5.jpg', 'chosen': false},
    {'id': 'oversized', 'name': 'Oversized', 'img': 'top 12.jpg', 'chosen': false},
    {'id': 'peplum', 'name': 'Peplum', 'img': 'top 6.jpg', 'chosen': false},
    {'id': 'pullover', 'name': 'Pullover', 'img': 'top 11.jpg', 'chosen': false},
    {'id': 'shirt', 'name': 'Shirt', 'img': 'top 9.jpg', 'chosen': false},
    {'id': 'turtleneck', 'name': 'Turtleneck', 'img': 'top 10.jpg', 'chosen': false},
  ];

  public arDress = [
    {'id': 'a_line', 'name': 'A-line', 'img': 'dress 10.jpg', 'chosen': false},
    {'id': 'assymetrical', 'name': 'Assymetrical', 'img': 'dress 2.jpg', 'chosen': false},
    {'id': 'caftan', 'name': 'Caftan', 'img': 'dress 8.jpg', 'chosen': false},
    {'id': 'chemise', 'name': 'Chemise', 'img': 'dress 12.jpg', 'chosen': false},
    {'id': 'draped', 'name': 'Draped', 'img': 'dress 15.jpg', 'chosen': false},
    {'id': 'drop_waist', 'name': 'Drop-waist', 'img': 'dress 17.jpg', 'chosen': false},
    {'id': 'empire_waist', 'name': 'Empire Waist', 'img': 'dress 3.jpg', 'chosen': false},
    {'id': 'empire', 'name': 'Empire', 'img': 'dress 16.jpg', 'chosen': false},
    {'id': 'jumper', 'name': 'Jumper', 'img': 'dress 18.jpg', 'chosen': false},
    {'id': 'maxi', 'name': 'Maxi', 'img': 'dress 19.jpg', 'chosen': false},
    {'id': 'pant_dress', 'name': 'Pant Dress', 'img': 'dress 13.jpg', 'chosen': false},
    {'id': 'princess', 'name': 'Princess', 'img': 'dress 5.jpg', 'chosen': false},
    {'id': 'sheath', 'name': 'Sheath', 'img': 'dress 1.jpg', 'chosen': false},
    {'id': 'shirtdress', 'name': 'Shirtdress', 'img': 'dress 9.jpg', 'chosen': false},
    {'id': 'sun_dress', 'name': 'Sun-dress', 'img': 'dress 4.jpg', 'chosen': false},
    {'id': 'tent', 'name': 'Tent', 'img': 'dress 14.jpg', 'chosen': false},
    {'id': 'trapeze', 'name': 'Trapeze', 'img': 'dress 7.jpg', 'chosen': false},
    {'id': 'wedge', 'name': 'Wedge', 'img': 'dress 6.jpg', 'chosen': false}
  ];

  public arJackets = [
    {'id': 'baseball', 'name': 'Baseball', 'img': 'jacket 7.jpg', 'chosen': false},
    {'id': 'blazer', 'name': 'Blazer', 'img': 'jacket 6.jpg', 'chosen': false},
    {'id': 'bomber', 'name': 'Bomber', 'img': 'jacket 4.jpg', 'chosen': false},
    {'id': 'cold_shoulder', 'name': 'Cold shoulder', 'img': 'jacket 9.jpg', 'chosen': false},
    {'id': 'chubby', 'name': 'Chubby', 'img': 'jacket 2.jpg', 'chosen': false},
    {'id': 'insulated', 'name': 'Insulated', 'img': 'jacket 3.jpg', 'chosen': false},
    {'id': 'parka', 'name': 'Parka', 'img': 'jacket 8.jpg', 'chosen': false},
    {'id': 'poncho', 'name': 'Poncho', 'img': 'jacket 10.jpg', 'chosen': false},
    {'id': 'shearling', 'name': 'Shearling', 'img': 'jacket 1.jpg', 'chosen': false},
    {'id': 'windbreaker', 'name': 'Windbreaker', 'img': 'jacket 5.jpg', 'chosen': false},
    {'id': 'anorak', 'name': 'Anorak', 'img': 'jacket 11.jpg', 'chosen': false},
    {'id': 'cropped Sweater', 'name': 'Cropped Sweater', 'img': 'sweater 2.jpg', 'chosen': false},
    {'id': 'fanny', 'name': 'Fanny', 'img': 'sweater 1.jpg', 'chosen': false},
    {'id': 'sweatshirt', 'name': 'Sweatshirt', 'img': 'sweater 4.jpg', 'chosen': false},
    {'id': 'tennis', 'name': 'Tennis', 'img': 'sweater 3.jpg', 'chosen': false},
  ];

  public arShorts = [
    {'id': 'active', 'name': 'Active', 'img': 'shorts 3.jpg', 'chosen': false},
    {'id': 'bermuda', 'name': 'Bermuda', 'img': 'shorts 5.jpg', 'chosen': false},
    {'id': 'cargo', 'name': 'Cargo', 'img': 'shorts 1.jpg', 'chosen': false},
    {'id': 'casual', 'name': 'Casual', 'img': 'shorts 7.jpg', 'chosen': false},
    {'id': 'chinos', 'name': 'Chinos', 'img': 'shorts 9.jpg', 'chosen': false},
    {'id': 'denim', 'name': 'Denim', 'img': 'shorts 6.jpg', 'chosen': false},
    {'id': 'romper', 'name': 'Romper', 'img': 'shorts 8.jpg', 'chosen': false},
  ];

  public arSkirts = [
    {'id': 'a_line', 'name': 'A-line', 'img': 'skirt 10.jpg', 'chosen': false},
    {'id': 'circular', 'name': 'Circular', 'img': 'skirt 5.jpg', 'chosen': false},
    {'id': 'draped', 'name': 'Draped', 'img': 'skirt 3.jpg', 'chosen': false},
    {'id': 'flared', 'name': 'Flared', 'img': 'skirt 7.jpg', 'chosen': false},
    {'id': 'gathered', 'name': 'Gathered', 'img': 'skirt 1.jpg', 'chosen': false},
    {'id': 'gored', 'name': 'Gored', 'img': 'skirt 2.jpg', 'chosen': false},
    {'id': 'granny', 'name': 'Granny', 'img': 'skirt 6.jpg', 'chosen': false},
    {'id': 'pencil', 'name': 'Pencil', 'img': 'skirt 8.jpg', 'chosen': false},
    {'id': 'tiered', 'name': 'Tiered', 'img': 'skirt 4.jpg', 'chosen': false},
  ];

  public arPants = [
    {'id': 'baggies', 'name': 'Baggies', 'img': 'pants 10.jpg', 'chosen': false},
    {'id': 'bell_bottoms', 'name': 'Bell Bottoms', 'img': 'pants 1.jpg', 'chosen': false},
    {'id': 'boot_cut', 'name': 'Boot cut', 'img': 'pants 7.jpg', 'chosen': false},
    {'id': 'harem', 'name': 'Harem', 'img': 'pants 5.jpg', 'chosen': false},
    {'id': 'jeans', 'name': 'Jeans', 'img': 'pants 11.jpg', 'chosen': false},
    {'id': 'jumpsuit', 'name': 'Jumpsuit', 'img': 'pants 8.jpg', 'chosen': false},
    {'id': 'knickers', 'name': 'Knickers', 'img': 'pants 6.jpg', 'chosen': false},
    {'id': 'palazzo', 'name': 'Palazzo', 'img': 'pants 2.jpg', 'chosen': false},
    {'id': 'straight', 'name': 'Straight', 'img': 'pants 4.jpg', 'chosen': false},
    {'id': 'tapered', 'name': 'Tapered', 'img': 'pants 3.jpg', 'chosen': false},
    {'id': 'toreador', 'name': 'Toreador', 'img': 'pants 9.jpg', 'chosen': false},
  ];

  public objLabels = {
    'tops': { label: 'Tops', list: this.arTops},
    'dresses': { label: 'Dresses', list: this.arDress},
    'jackets': { label: 'Jackets & Sweaters', list: this.arJackets},
    'shorts': { label: 'Shorts', list: this.arShorts},
    'skirts': { label: 'Skirts', list: this.arSkirts},
    'pants': { label:  'Pants', list: this.arPants}
  }

  constructor(
    public registerService: RegisterService,
    private route: ActivatedRoute
  ) {
      this.route.params.subscribe(params => {
        this.strTag = params.tag; 
      });
    }

  ngOnInit() {

    this.bShow = false;

    let objCurrent = this.registerService.getCurrentRegisterObject();
    let strKey = 'common_' + this.strTag;

    if (objCurrent[strKey] != undefined) {
      objCurrent[strKey].forEach(item => {
        
        let numIfFound = this.objLabels[this.strTag]['list'].findIndex(look => (look.id == item.id));

        if (numIfFound != -1) {
          this.objLabels[this.strTag]['list'][numIfFound]['chosen'] = true;
        }
        
      });
    }

    setTimeout(() => {
      this.bShow = true;
    }, 1000);
  }

  chooseThis(chosenObj) {
    let objId = this.objLabels[this.strTag]['list'].findIndex(item => (item.id == chosenObj.id));

    if (objId != -1) {
      if (this.objLabels[this.strTag]['list'][objId]['chosen']) {
        this.objLabels[this.strTag]['list'][objId]['chosen'] = false;
      } else {
        this.objLabels[this.strTag]['list'][objId]['chosen'] = true;
      }
    }
  }

  saveData() {

    let arChosed = [];
    
    this.objLabels[this.strTag]['list'].forEach(item => {
      if (item['chosen']) {
        arChosed.push(item);
      }
    });

    let strKey = 'common_' + this.strTag;

    this.registerService.mergeData({[strKey]:  arChosed});

  }

}
