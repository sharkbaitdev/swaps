import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RegisterPageRoutingModule } from './register.router.module';
import { RegisterService } from '../a-services/register.service';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RegisterPageRoutingModule
  ],
  declarations: [],
  providers: [
    RegisterService
  ]
})
export class RegisterPageModule {}
