import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../../a-services/register.service';

@Component({
  selector: 'app-signup2',
  templateUrl: './signup2.page.html',
  styleUrls: ['./signup2.page.scss'],
})
export class Signup2Page implements OnInit {

  public objData = {
    top_size: '',
    bottom_size: '',
  }

  constructor(public registerService: RegisterService) { }

  ngOnInit() {}

  saveData() {
    this.registerService.mergeData(this.objData);
  }

}
