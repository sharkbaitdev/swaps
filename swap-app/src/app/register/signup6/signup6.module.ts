import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Signup6Page } from './signup6.page';

import { MatButtonModule, MatIconModule, MatExpansionModule, MatInputModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: Signup6Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Signup6Page]
})
export class Signup6PageModule {}
