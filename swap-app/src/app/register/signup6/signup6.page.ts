import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../../a-services/register.service';

@Component({
  selector: 'app-signup6',
  templateUrl: './signup6.page.html',
  styleUrls: ['./signup6.page.scss'],
})
export class Signup6Page implements OnInit {

  panelOpenState = false;

  public bOpenDetail1 = false;
  public bOpenDetail2 = false;

  public arDetail1 = [
    {id: 'shoulder_cut', name: 'Shoulder cut'},
    {id: 'necklines', name: 'Neckline'},
    {id: 'sleeve_length', name: 'Sleeve length'}
  ];

  public arDetail2 = [
    {id: 'skirt_length', name: 'Skirt length'},
  ];

  constructor(public registerService: RegisterService) { }
  ngOnInit() {
  }

  toggleDetail1() {
    this.bOpenDetail1 = (this.bOpenDetail1) ? false : true;
  }

  toggleDetail2() {
    this.bOpenDetail2 = (this.bOpenDetail2) ? false : true;
  }

  register() {
    this.registerService.register().subscribe(data => {
      console.log(data);
    })
  }
}
