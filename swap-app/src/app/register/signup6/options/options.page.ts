import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-options',
  templateUrl: './options.page.html',
  styleUrls: ['./options.page.scss'],
})
export class OptionsPage implements OnInit {

  public test = '';

  constructor(  private router: Router,
    private route: ActivatedRoute) {
      this.route.params.subscribe(params => {
        this.test = params.tag; 
        console.log(this.test);
      });
    }

  ngOnInit() {
  }

}
