import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signup6Page } from './signup6.page';

describe('Signup6Page', () => {
  let component: Signup6Page;
  let fixture: ComponentFixture<Signup6Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signup6Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signup6Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
