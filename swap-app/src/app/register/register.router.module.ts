import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/register/step1',
    pathMatch: 'full'
  },
  {
    path: 'done',
    children: [
      { 
        path: '', 
        loadChildren: './done/done.module#DonePageModule' },
    ]
  },
  {
    path: 'step1',
    children: [
      {
        path: '',
        loadChildren: './signup1/signup1.module#Signup1PageModule'
      }
    ]
  },
  {
    path: 'step2',
    children: [
      {
        path: '',
        loadChildren: './signup2/signup2.module#Signup2PageModule'
      }
    ]
  },
  {
    path: 'step3',
    children: [
      {
        path: '',
        loadChildren: './signup3/signup3.module#Signup3PageModule'
      },
      { 
        path: 'choose/:tag', 
        loadChildren: './signup3/choose/choose.module#ChoosePageModule'
      },
    ]
  },
  {
    path: 'step4',
    children: [
      {
        path: '',
        loadChildren: './signup4/signup4.module#Signup4PageModule'
      }
    ]
  },
  {
    path: 'step5',
    children: [
      {
        path: '',
        loadChildren: './signup5/signup5.module#Signup5PageModule'
      }
    ]
  },
  {
    path: 'step6',
    children: [
      {
        path: '',
        loadChildren: './signup6/signup6.module#Signup6PageModule'
      },
      {
        path: 'options/:tag',
        loadChildren: './signup6/options/options.module#OptionsPageModule'
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class RegisterPageRoutingModule {}
