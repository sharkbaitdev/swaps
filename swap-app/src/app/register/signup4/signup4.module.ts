import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Signup4Page } from './signup4.page';

import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: Signup4Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Signup4Page]
})
export class Signup4PageModule {}
