import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../../a-services/register.service';

@Component({
  selector: 'app-signup4',
  templateUrl: './signup4.page.html',
  styleUrls: ['./signup4.page.scss'],
})
export class Signup4Page implements OnInit {

  public objData = {
    occassions: '',
    colors: '',
    style: '',
  }

  constructor(public registerService: RegisterService) { }

  ngOnInit() {}

  saveData() {
    this.registerService.mergeData(this.objData);
  }

}
