import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  public test = ""
  public arMessage = [
    {name: "Elle Baquir", content: "This is a test.", date: "2:34 AM"},
    {name: "Tin Pelingon", content: "This is a test.", date: "4:21 AM"}
  ];
  constructor() { }

  ngOnInit() {
  }

}
