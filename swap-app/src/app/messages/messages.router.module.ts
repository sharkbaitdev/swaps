import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'thread',
    children: [
      {
        path: '',
        loadChildren: './thread/thread.module#ThreadPageModule'
      }
    ]
  },
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MessagesPageRoutingModule {}
