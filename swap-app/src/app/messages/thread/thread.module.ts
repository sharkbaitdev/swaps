import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ThreadPage } from './thread.page';
import { MatIconModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: ThreadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatIconModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ThreadPage]
})
export class ThreadPageModule {}
