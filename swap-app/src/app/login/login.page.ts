import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../a-services/register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public objData = {
    'name' : '',
    'password' : ''
  }

  public strLink = '/login'

  constructor(public accountService : RegisterService, private router: Router) { }

  ngOnInit() {}

  login() {

    if (this.objData['login'] == "" || this.objData['login'] == "") {
        alert("Username or password cannot be empty.")
    }
    this.accountService.login(this.objData).subscribe(data => {
        console.log(data);

        if (data['success'] == false) {
            alert("Username or password cannot be empty.")
        } else {
            this.accountService.setUserData(data['data']);
            this.router.navigate(['/profile']);
        }
    });
  }

}
