<?php

use Slim\Http\Request;
use Slim\Http\Response;


/**

 * Swaps Fashion App

 */

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {

    // Sample log message

    $this->logger->info("Swaps Fashion App '/' route");

    // Render index view

    return $this->renderer->render($response, 'index.phtml', $args);

});

