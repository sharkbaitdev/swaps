<?php

use Slim\Http\Request;
use Slim\Http\Response;


/**

 * Olivarez College Feedback System

 */

$app->get('/ocf/account/all/[{type}]', function (Request $request, Response $response, array $args) {

    $arAllowedTypes = array("teacher", "student", "personnel", "guest", "admin");

    if (in_array($args['type'], $arAllowedTypes)) {
        $sth = $this->ocdb->prepare("SELECT * FROM tbl_accounts WHERE user_type = :type");
        $sth->execute($args);
        $arResult = $sth->fetchAll();
        return $this->response->withJson($arResult);
    }

    if (!isset($args['type'])) {
        $sth = $this->ocdb->prepare("SELECT * FROM tbl_accounts");
        $sth->execute();
        $arResult = $sth->fetchAll();
        return $this->response->withJson($arResult);
    }

    $arResponse = array(
        'success' => false,
        'message' => "The type you want to list is invalid."
    );

    return $this->response->withJson($arResponse);

});


$app->get('/ocf/account/delete/[{id}]', function (Request $request, Response $response, array $args) {

    $sth = $this->ocdb->prepare("DELETE FROM tbl_accounts WHERE id = :id");
    $sth->execute($args);

    $arResponse = array(
        'success' => true,
        'message' => "Account has been deleted."
    );

    return $this->response->withJson($arResponse);

});

$app->get('/ocf/post/delete/[{id}]', function (Request $request, Response $response, array $args) {

    $sth = $this->ocdb->prepare("DELETE FROM tbl_posts WHERE id = :id");
    $sth->execute($args);

    $arResponse = array(
        'success' => true,
        'message' => "Post has been deleted."
    );

    return $this->response->withJson($arResponse);

});



$app->post('/ocf/account/save', function (Request $request, Response $response) {

    $objAccountDetails = json_decode($request->getParsedBodyParam('objAccountDetails', $default = null), true);

    $strPart = "UPDATE tbl_accounts SET ";

    if (isset($objAccountDetails['password'])) {
         $objAccountDetails['password'] = md5($objAccountDetails['password']);
    }

    foreach ($objAccountDetails as $key => $value) {
        if ($key != 'id') {
            $strPart .= $key." = :".$key.", ";
        }
    }

    $strPart = rtrim($strPart, ", ");
    $strPart .= " WHERE id = :id";

    $sth = $this->ocdb->prepare($strPart);
    $sth->execute($objAccountDetails);

    $arResponse = array(
        'success' => true,
        'message' => "Account updates have been saved."
    );

    return $this->response->withJson($arResponse);

});


$app->post('/ocf/login', function (Request $request, Response $response) {

    $objAccountDetails = json_decode($request->getParsedBodyParam('objAccountDetails', $default = null), true);

    $strUsername = $objAccountDetails['username'];
    $strPassword = md5($objAccountDetails['password']);
    //$strPassword = $objAccountDetails['password'];

    $sth = $this->ocdb->prepare("SELECT * FROM tbl_accounts WHERE username = :strusername AND password = :strpassword");

    $sth->execute(array(
        'strusername' => $strUsername,
        'strpassword' => $strPassword,
    ));

    $arResult = $sth->fetchAll();

    $arResponse = array(
        'success' => false,
        'message' => "That account doesn't seem to exist."
    );

    if (count($arResult) > 0) {

        $arResponse = array(
            'success' => true,
            'message' => "You have successfully loged in.",
            'data' => $arResult[0]
        );

    }

    return $this->response->withJson($arResponse);

});



$app->post('/ocf/register', function (Request $request, Response $response) {
    

    $objAccountDetails = json_decode($request->getParsedBodyParam('objAccountDetails', $default = null), true);
    
    $strQuery = "";
    $arQuery = array(
        "strUsername" => $objAccountDetails['username'],
        );
        
    //Check first if the username already exists.
    /*if ($objAccountDetails['email'] != "") {
            $strQuery = "SELECT * FROM tbl_accounts WHERE username = :strUsername OR email = :strEmail";
            array_push($arQuery, ["strEmail" => $objAccountDetails['email']]);
    } else {*/
        $strQuery = "SELECT * FROM tbl_accounts WHERE username = :strUsername";
    //}

    $sth = $this->ocdb->prepare($strQuery);
    
    $sth->execute($arQuery);

    $arResult = $sth->fetchAll();

    if (count($arResult) > 0) {
        $arResponse = array(
            'success' => false,
            'message' => "The username already exist, please enter another one."
        );

        return $this->response->withJson($arResponse);
    }
    

    // If not yet exist, continue.
    $objAccountDetails['password'] = md5($objAccountDetails['password']);
    $objAccountDetails['is_accepted'] = 0;
    $objAccountDetails['date_created'] = date('Y-m-d H:i:s');
    
    //return $this->response->withJson($objAccountDetails);
    
    
    $sth1 = $this->ocdb->prepare("
        INSERT INTO tbl_accounts(first_name, last_name, username, password, email, phone, address, user_type, department, image_file, is_accepted) 
        VALUES(:first_name, :last_name, :username, :password, :email, :phone, :address, :user_type, :department, :image_file, :is_accepted)
    ");
    

    $sth1->execute([
        'first_name' => $objAccountDetails['first_name'],
        'last_name' => $objAccountDetails['last_name'],
        'username' => $objAccountDetails['username'],
        'password' => $objAccountDetails['password'],
        'email' => $objAccountDetails['email'],
        'phone' => $objAccountDetails['phone'],
        'address' => $objAccountDetails['address'],
        'user_type' => $objAccountDetails['user_type'],
        'department' => $objAccountDetails['department'],
        'image_file' => $objAccountDetails['image_file'],
        'is_accepted' => $objAccountDetails['is_accepted'],
    ]);
    
    
    $arResponse = array(
        'success' => true,
        'message' => "The account was successfully registered."
    );

    return $this->response->withJson($arResponse);

});


/* id
message
viewers_type
admin_id
date_posted */

$app->post('/ocf/post/create', function (Request $request, Response $response) {

    $objPostDetails = json_decode($request->getParsedBodyParam('objPostDetails', $default = null), true);

    $objPostDetails['date_posted'] = date('Y-m-d H:i:s');
    $sth = $this->ocdb->prepare("
        INSERT INTO tbl_posts (message, viewers_type, admin_id, subject, admin_name, date_posted) 
        VALUES(:message, :viewers_type, :admin_id, :subject, :admin_name, :date_posted)
    ");
    
    $sth->execute($objPostDetails);

    $arResponse = array(
        'success' => true,
        'message' => "The post was successfully registered."
    );

    return $this->response->withJson($arResponse);

});



$app->get('/ocf/post/[{viewers_type}]', function (Request $request, Response $response, array $args) {

    if($args['viewers_type'] === 'all') {
        $sth = $this->ocdb->prepare("SELECT * FROM tbl_posts");
        $sth->execute();
    } else {
        $sth = $this->ocdb->prepare("SELECT * FROM tbl_posts WHERE viewers_type = :viewers_type OR viewers_type = 'all'");
        $sth->execute($args);
    }

    $arResult = $sth->fetchAll();
    return $this->response->withJson($arResult);

});


$app->post('/ocf/post/save', function (Request $request, Response $response) {

    $objPostDetails = json_decode($request->getParsedBodyParam('objPostDetails', $default = null), true);

    $strPart = "UPDATE tbl_posts SET ";

    foreach ($objPostDetails as $key => $value) {
        if ($key != 'id') {
            $strPart .= $key." = :".$key.", ";
        }
    }

    $strPart = rtrim($strPart, ", ");
    $strPart .= " WHERE id = :id";

    $sth = $this->ocdb->prepare($strPart);
    $sth->execute($objPostDetails);

    $arResponse = array(
        'success' => true,
        'message' => "Your post was successfully updated."
    );

    return $this->response->withJson($arResponse);

});

/**
 *  "message":
 *  "thread_id"
 *  "sender_id"
 *  "sender_name"
 *  "sender_type"
 *  "receiver_id"
 *  "receiver_type"
 *  "message_type": [ "parent", "child" ]
 *  "date_sent": "2014-06-10 13:23:23"
 */


$app->post('/ocf/feedback/create', function (Request $request, Response $response) {

    $objFeedbackDetails = json_decode($request->getParsedBodyParam('objFeedbackDetails', $default = null), true);

    $stmt = $this->ocdb->prepare("SELECT count(*) FROM tbl_feedback WHERE thread_id = ?");
    $stmt->execute([$objFeedbackDetails['thread_id']]);
    $threadCount = $stmt->fetchColumn();
    
    // If thread_id is already existing, Just add message to existing thread and flag as child
    if ($threadCount) {
        $objFeedbackDetails['message_type'] = 'child';
    } else {
        $objFeedbackDetails['message_type'] = 'parent';
        $objFeedbackDetails['thread_id'] = time();
    }

    $objFeedbackDetails['date_sent'] = date('Y-m-d H:i:s');

    $sth = $this->ocdb->prepare("
        INSERT INTO tbl_feedback (message, thread_id, sender_id, sender_name,
        sender_type, receiver_id, receiver_type, message_type, date_sent, sender_image)
        VALUES(:message, :thread_id, :sender_id, :sender_name,
        :sender_type, :receiver_id, :receiver_type, :message_type, :date_sent, :sender_image)
    ");

    // If message is inserted Get all messages from current thread
    if ($sth->execute($objFeedbackDetails)) {

        $currentThread = $this->ocdb->prepare("SELECT * FROM tbl_feedback WHERE thread_id = ? ORDER BY date_sent ASC");
        $currentThread->execute([$objFeedbackDetails['thread_id']]);

        $arResult = $currentThread->fetchAll();

        $arResponse = array(
            'success' => true,
            'thread' => $arResult[0]['thread_id'],
            'message' => "Message successfuly sent."
        );
    } else {

        $arResponse  = array(
            'success' => false,
           'message' => "Error in Sending message. Try again later."
        );
    }

    return $this->response->withJson($arResponse);

});



// fetch all latest message of feedback just supply the sender_id. sender type is filter for view by user type. supply empty character if no filter is applied
$app->get('/ocf/feedback/fetchall/{sender_id}/[{sender_type}]', function (Request $request, Response $response, array $args) {
 
    $strSenderType = isset($args['sender_type']) ? $args['sender_type'] : '';
    
    if ($strSenderType !== '') {
        
        if ($strSenderType != 'All') {
            $sth = $this->ocdb->prepare("SELECT *, sender_name as parent_name FROM tbl_feedback WHERE sender_type = '".$strSenderType."' AND message_type = 'parent'");
        } else {
            $sth = $this->ocdb->prepare("SELECT *, sender_name as parent_name FROM tbl_feedback WHERE message_type = 'parent'");
        }



    } else {
        
        if ($args['sender_id'] == 1) {
            $sth = $this->ocdb->prepare("SELECT *, sender_name as parent_name FROM tbl_feedback WHERE message_type = 'parent'");
        } else {
            $sth = $this->ocdb->prepare("SELECT *, sender_name as parent_name FROM tbl_feedback WHERE sender_id = ".$args['sender_id']." AND message_type = 'parent'");
        }
        
    }
    
    /*$strOrder = isset($args['order_by']) ? $args['order_by'] : 'date_sent';
    $strOrderBy = $strOrder === 'date_sent' ? 'DESC' : 'ASC';

    if ($args['sender_id'] == 1) {
        // ORDER BY IS APPLICABLE IN ADMIN ONLY
        $sth = $this->ocdb->prepare("SELECT *, sender_name as parent_name FROM tbl_feedback WHERE message_type = 'parent' ORDER BY $strOrder $strOrderBy");
    } else {
        $sth = $this->ocdb->prepare("SELECT *, sender_name as parent_name FROM tbl_feedback WHERE sender_id = :sender_id AND message_type = 'parent'");
    }*/

    $sth->execute($args);
    $arResult = $sth->fetchAll();

    
    return $this->response->withJson($arResult);

});


/*
$app->get('/ocf/feedback/fetchall/{sender_id}/[{sender_type}]', function (Request $request, Response $response, array $args) {
 
    if (isset($args['sender_type'])) {

        $sth = $this->ocdb->prepare("SELECT thread_id FROM tbl_feedback WHERE sender_id = :sender_id AND sender_type = :sender_type AND message_type = 'parent'");

    } else {
        
        if ($args['sender_id'] == 1) {
            $sth = $this->ocdb->prepare("SELECT thread_id FROM tbl_feedback WHERE receiver_id = :sender_id AND message_type = 'parent'");
        } else {
            $sth = $this->ocdb->prepare("SELECT thread_id FROM tbl_feedback WHERE sender_id = :sender_id AND message_type = 'parent'");
        }
        
    }

    $sth->execute($args);
    $arResult = $sth->fetchAll();

    if(count($arResult) < 1) {
        return $this->response->withJson(
            []
        );
    }
  

    $arThreadId = [];

    foreach ($arResult as $strThreadId) {
        array_push( $arThreadId,  $strThreadId['thread_id'] );
    }

    $threadIds = str_repeat('?,', count($arThreadId) - 1) . '?';
    $stm = $this->ocdb->prepare("SELECT max(date_sent) as date_sent FROM tbl_feedback WHERE thread_id IN ($threadIds) GROUP BY thread_id");
    $stm->execute($arThreadId);
    $maxDates = $stm->fetchAll();

    $arMaxDates = [];

    foreach ($maxDates as $strMaxDates) {
        array_push( $arMaxDates,  $strMaxDates['date_sent'] );
    }

    $maxDateCount = str_repeat('?,', count($arMaxDates) - 1) . '?';
    $x = $this->ocdb->prepare("SELECT * FROM tbl_feedback WHERE date_sent IN ($maxDateCount)");
    $x->execute($arMaxDates);
    $feedsWithMaxDate = $x->fetchAll();
    
    foreach ($feedsWithMaxDate as $key => $value) {
        if ($feedsWithMaxDate[$key]['message_type'] != 'parent') {
            $parent1 = $this->ocdb->prepare("SELECT * FROM tbl_feedback WHERE thread_id = :thread_id AND message_type = 'parent'");
            $parent1->execute(array('thread_id' => $feedsWithMaxDate[$key]['thread_id']));
            $feedsWithMaxDate[$key]['parent_name'] = $parent1->fetchAll()[$key]['sender_name'];    
        } else {
            $feedsWithMaxDate[$key]['parent_name'] = $feedsWithMaxDate[$key]['sender_name'];
        }
    }
    
    return $this->response->withJson($feedsWithMaxDate);

});

*/


// Get all messages from the thread just supply thread id
$app->get('/ocf/approved/{id}', function (Request $request, Response $response, array $args) {

    $sth = $this->ocdb->prepare("SELECT * FROM tbl_accounts WHERE id = :id");
    $sth->execute($args);
    $arResult = $sth->fetchAll();
    
    return $this->response->withJson($arResult[0]);

});

// Get all messages from the thread just supply thread id
$app->get('/ocf/feedback/{thread_id}', function (Request $request, Response $response, array $args) {

    $sth = $this->ocdb->prepare("SELECT * FROM tbl_feedback WHERE thread_id = :thread_id ORDER BY date_sent ASC");
    $sth->execute($args);
    $arResult = $sth->fetchAll();

    return $this->response->withJson($arResult);

});


$app->get('/ocf/reporting', function (Request $request, Response $response, array $args) {
 
     /* $queryStudent = $this->ocdb->prepare("SELECT count(*) FROM tbl_feedback WHERE receiver_type = 'admin' AND message_type = 'parent' AND sender_type = 'student'");
    $queryStudent->execute();
    $studentCount = $queryStudent->fetchColumn();

    $queryTeacher = $this->ocdb->prepare("SELECT count(*) FROM tbl_feedback WHERE receiver_type = 'admin' AND message_type = 'parent' AND sender_type = 'teacher'");
    $queryTeacher->execute();
    $teacherCount = $queryTeacher->fetchColumn();

    $queryPersonnel = $this->ocdb->prepare("SELECT count(*) FROM tbl_feedback WHERE receiver_type = 'admin' AND message_type = 'parent' AND sender_type = 'personnel'");
    $queryPersonnel->execute();
    $personnelCount = $queryPersonnel->fetchColumn();

    $queryGuest = $this->ocdb->prepare("SELECT count(*) FROM tbl_feedback WHERE receiver_type = 'admin' AND message_type = 'parent' AND sender_type = 'guest'");
    $queryGuest->execute();
    $guestCount = $queryGuest->fetchColumn(); */

    $queryStudent = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE user_type = 'Student'");
    $queryStudent->execute();
    $studentCount = $queryStudent->fetchColumn();

    $queryTeacher = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE user_type = 'Teacher'");
    $queryTeacher->execute();
    $teacherCount = $queryTeacher->fetchColumn();

    $queryPersonnel = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE user_type = 'School Personnel'");
    $queryPersonnel->execute();
    $personnelCount = $queryPersonnel->fetchColumn();
    
    $queryParent = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE user_type = 'Parent'");
    $queryParent->execute();
    $parentCount = $queryParent->fetchColumn();

    $queryGuest = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE user_type = 'Guest'");
    $queryGuest->execute();
    $guestCount = $queryGuest->fetchColumn();

    $query5stars = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE app_rating = 5");
    $query5stars->execute();
    $star_5 = $query5stars->fetchColumn();

    $query4stars = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE app_rating = 4");
    $query4stars->execute();
    $star_4 = $query4stars->fetchColumn();

    $query3stars = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE app_rating = 3");
    $query3stars->execute();
    $star_3 = $query3stars->fetchColumn();

    $query2stars = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE app_rating = 2");
    $query2stars->execute();
    $star_2 = $query2stars->fetchColumn();

    $query1stars = $this->ocdb->prepare("SELECT count(*) FROM tbl_accounts WHERE app_rating = 1");
    $query1stars->execute();
    $star_1 = $query1stars->fetchColumn();

    $arResponse  = array(
        'student'   => $studentCount,
        'teacher'   => $teacherCount,
        'personnel' => $personnelCount,
        'parent'    => $parentCount,
        'guest'     => $guestCount,
        '5_stars'   => $star_5,
        '4_stars'   => $star_4,
        '3_stars'   => $star_3,
        '2_stars'   => $star_2,
        '1_star'    => $star_1

    );

    return $this->response->withJson($arResponse);

});


$app->post('/ocf/account/rating', function (Request $request, Response $response) {

    $objAccountDetails = json_decode($request->getParsedBodyParam('objAccountDetails', $default = null), true);

    $sth = $this->ocdb->prepare("SELECT app_rating from tbl_accounts WHERE id = :id");
    $sth->execute(['id' => $objAccountDetails['id']]);
    $arResult = $sth->fetch();

    if ($arResult['app_rating'] == 0) {

        $updateRating = $this->ocdb->prepare("UPDATE tbl_accounts SET app_rating = :app_rating WHERE id = :id");
        $updateRating->execute($objAccountDetails);
        $arResponse = array(
            'success' => true,
            'message' => "Your rating has been sent."
        );
    } else {
        $arResponse = array(
            'success' => false,
            'message' => "You already have casted your rating."
        );
    }

    return $this->response->withJson($arResponse);

});

$app->get('/ocf/account/[{id}]', function (Request $request, Response $response, array $args) {

    $sth = $this->ocdb->prepare("SELECT * FROM tbl_accounts WHERE id = :id");
    $sth->execute($args);

    $arResult = $sth->fetch();

    return $this->response->withJson($arResult);

});



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**

 * Chronorca

 */

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//TODO: Transfer to controller.

$app->get('/items', function (Request $request, Response $response) {

    $sth = $this->db->prepare("SELECT * FROM items ORDER BY item_name ASC");

    $sth->execute();

    $todos = $sth->fetchAll();

    return $this->response->withJson($todos);

});



$app->post('/item/delete', function (Request $request, Response $response) {



    $arResponse = array(

        'message' => "Item was successfully deleted!"

    );



    $itemData = json_decode($request->getParsedBodyParam('itemData', $default = null));



    $strDirectory = __DIR__ . '/../public/images';



    $file = $strDirectory. "/" . $itemData->item_type . DIRECTORY_SEPARATOR . $itemData->item_id . ".png";



    $sth = $this->db->prepare("DELETE FROM items WHERE item_id = :itemid");

    $arResult = $sth->execute(array('itemid' => $itemData->item_id));



    if (!$arResult) {

        $arResponse['message'] = "Item could not be deleted in the database.";

    } else {

        if (!unlink($file)) {

            $arResponse['message'] = "Image deletion has failed, please try again.";

        }

    }

    

    return $this->response->withJson($arResponse);

});



$app->post('/item/create', function(Request $request, Response $response) {

    //Where the directory will be.

    $strDirectory = __DIR__ . '/../public/images';



    $arResponse = array(

        'message' => "Item was successfully added to the database!"

    );



    $itemData = json_decode($request->getParsedBodyParam('itemData', $default = null));

    $uploadedFiles = $request->getUploadedFiles();

    $arLayers = [];

    $itemData->id = uniqid();



    foreach ($uploadedFiles as $uploadedFileIndex => $uploadedFileValue) {

        if ($uploadedFileValue->getError() === UPLOAD_ERR_OK) {



            $extension = pathinfo($uploadedFileValue->getClientFilename(), PATHINFO_EXTENSION);



            if ($uploadedFileIndex == 'itemThumbnail') {

                $uploadedFileValue->moveTo($strDirectory. "/" . 'thumbnails' . DIRECTORY_SEPARATOR . $itemData->id . "." . $extension);

            } else {

                $uploadedFileValue->moveTo($strDirectory. "/" . $itemData->type . DIRECTORY_SEPARATOR . $uploadedFileIndex. "_" .$itemData->id . "." . $extension);



                $layer['layer_src'] = "images/". $itemData->type . "/" . $uploadedFileIndex. "_" .$itemData->id . "." . $extension;

                

                $layer['layer_type'] = array(

                    'parent' => $itemData->type,

                    'type' => $uploadedFileIndex

                );



                switch ($uploadedFileIndex) {

                    case 'eye':

                        $layer['layer_num'] = 5;

                        break;

                    case 'eye_brow':

                        $layer['layer_num'] = 4;

                        break;

                    case 'lips':

                        $layer['layer_num'] = 7;

                        break;

                    case 'earrings':

                        $layer['layer_num'] = 16;

                        break;

                    case 'ring':

                        $layer['layer_num'] = 10;

                        break;

                    case 'necklace':

                        $layer['layer_num'] = 15;

                        break;

                    case 'bracelet':

                        $layer['layer_num'] = 9;

                        break;

                    case 'dress':

                        $layer['layer_num'] = 14;

                        break;

                    case 'full':

                        $layer['layer_num'] = 8;

                        break;

                    default:

                        if ($itemData->type == 'hair') {

                            

                            switch ($uploadedFileIndex) {

                                case 'front':

                                    $layer['layer_num'] = 19;

                                    break;

                                case 'back':

                                    $layer['layer_num'] = 1;

                                    break;

                            }

                        }

                        if ($itemData->type == 'coat') {

                            

                            switch ($uploadedFileIndex) {

                                case 'front':

                                    $layer['layer_num'] = 17;

                                    break;

                                case 'back':

                                    $layer['layer_num'] = 2;

                                    break;

                            }

                        }

                        if ($itemData->type == 'top') {

                            $layer['layer_num'] = 13;

                        }

                        if ($itemData->type == 'bottom') {

                            $layer['layer_num'] = 12;

                        }

                        if ($itemData->type == 'feet') {

                            if ($itemData->compatibility[1]->underlay_bottom) {

                                $layer['layer_num'] = 11;

                            } else {

                                $layer['layer_num'] = 18;

                            }

                        }

                        break;

                }

                array_push($arLayers, $layer);

            }



        }

    }



    $sth = $this->db->prepare("

        INSERT INTO items(item_id, item_type, item_name, item_description, item_thumbnail, item_gender, item_layers, item_compatibility) 

        VALUES(:itemid, :itemtype, :itemname, :itemdesc, :itemthmb, :itemgender, :itemlayers, :itemcompat)

    ");



    $arResult = $sth->execute(array(

        "itemid" => $itemData->id,

        "itemtype" => $itemData->type,

        "itemname" => $itemData->name,

        "itemdesc" => $itemData->description,

        "itemthmb" => "images/thumbnails/".$itemData->id.".jpg",

        "itemgender" => $itemData->gender,

        "itemlayers" => json_encode($arLayers),

        "itemcompat" => (isset($itemData->compatibility)) ? json_encode($itemData->compatibility) : json_encode([]),

    ));



    if (!$arResult) {

        //TODO: If the database insert fails, image file should be delete.

        $arResponse['message'] = "Item could not be added.";

    }



    return $this->response->withJson($arResponse);

});





// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {

    // Sample log message

    $this->logger->info("Slim-Skeleton '/' route");



    // Render index view

    return $this->renderer->render($response, 'index.phtml', $args);

});

